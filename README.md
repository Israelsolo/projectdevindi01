## How to run the pages
- Step 1. Download all the files and put them on your desktop or Clone it from ( git clone https://bitbucket.org/Israelsolo/projectdevindi01.git)
- Step 2. Launch the "default" page using browser like chrome and firefox
- Step 3. Navigate between pages. ex from home page to dodo bird on the page you just opened

## License and Copy right
I choose the GNU General License because i don't mind people to use my codes as they like.
It keeps me away from liability and warrenty. Read more about the license under **[GNU General License](License).